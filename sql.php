<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>


<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          
        </li>
        <li class="nav-item">
          <a class="nav-link" href="add.php">Add-blog</a>
        </li>
        </ul>
      </div>
    </div>
  </nav>




  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/post-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1><?php
                    $servername = "localhost";
                    $username = "root";
                    $password = "user0123";
                    try {
                    $pdo = new PDO("mysql:host=$servername;dbname=myDB", $username, $password);
                    $idd = $_GET['id'];
                    $sql = "SELECT *
                              FROM blog
                              where id =$idd";         
                    $q = $pdo->query($sql);
                    $q->setFetchMode(PDO::FETCH_ASSOC);
                    } catch (PDOException $e) {
                    die("Could not connect to the database $dbname :" . $e->getMessage());
                    }
                    $row = $q->fetch();
                    $val=$row["id"];
                    echo '
                            <h2 class="post-title">'.$row["Title"].'</h2>
                            <h6 class="post-meta">Posted by
                            <a href="#">Start Bootstrap</a>
                            on '.$row["Date"].'</h6>
                            ';
                        if(filter_var($val,FILTER_VALIDATE_INT)){   
                        $stmt2=$pdo->prepare("SELECT tag.Tag,tag.tid FROM reltab,tag WHERE reltab.id = ? AND tag.tid=reltab.tid");
                        $stmt2->execute([$val]);}
                        echo "Tags:";
                        while ($row2 = $stmt2->fetch()) {
                          echo '<a href = "tag.php?tag='.$row2['tid'].'">'.$row2['Tag'].",".'</a>';
                         } 
                        echo'<hr>';
                    ?>
                    </h1>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
        <?php
                    $servername = "localhost";
                    $username = "root";
                    $password = "user0123";
                    try {
                    $pdo = new PDO("mysql:host=$servername;dbname=myDB", $username, $password);
                    $idd = $_GET['id'];
                    $sql = "SELECT *
                              FROM blog
                              where id =$idd";         
                    $q = $pdo->query($sql);
                    $q->setFetchMode(PDO::FETCH_ASSOC);
                    } catch (PDOException $e) {
                    die("Could not connect to the database $dbname :" . $e->getMessage());
                    }
                    $row = $q->fetch();
                    echo '
                            <p class="post-title">'.$row["Content"].'</p>
                            ';
                    ?>
            
        </div>
      </div>
    </div>
  </article>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
